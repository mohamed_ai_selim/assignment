#-------------------------------------------------
#
# Project created by QtCreator 2017-12-12T22:37:40
#
#-------------------------------------------------
QT += opengl
QT += widgets
QT += core gui
QT -= quick
CONFIG += c++14
CONFIG += exceptions

# please change this accordingly to where is the development OpenSceneGraph folder
win32 {
    OSG = 'C:\Development\Libraries\OpenSceneGraph-3.4.0'
    #OSG = 'C:\Development\Libraries\OSG-GL3-3.4.1'
    #GL3 = 'C:\Development\Libraries\GL3'
}

macx {
    OSG = '/usr/local'
}

unix:!macx {
    OSG = '/usr/local'
}


TARGET = assignment
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH +=$$OSG/include
INCLUDEPATH +=$$GL3/include

SOURCES += \
        main.cpp \
    core/CoreUtilities.cpp \
    core/CoreApplication.cpp \
    core/scopes/GlobalScope.cpp \
    core/events/Event.cpp \
    core/events/EventQueue.cpp \
    core/events/EventManager.cpp \
    core/events/EventPublisher.cpp \
    core/events/EventDispatcher.cpp \
    view/SceneViewGL.cpp \
    render/osgRenderer.cpp \
    render/Utilities.cpp \
    core/events/UpdateEvent.cpp \
    simulation/SimulationController.cpp \
    widget/MainWidget.cpp \
    simulation/Objects.cpp \
    processor/CollisionProcessor.cpp


HEADERS += \
    core/CoreUtilities.h \
    core/CoreApplication.h \
    core/scopes/GlobalScope.h \
    core/events/EventPublisher.h \
    core/events/EventQueue.h \
    core/events/EventManager.h \
    core/events/Event.h \
    core/events/EventDispatcher.h \
    view/SceneViewGL.h \
    render/osgRenderer.h \
    render/Utilities.h \
    core/events/UpdateEvent.h \
    simulation/SimulationController.h \
    widget/MainWidget.h \
    simulation/Objects.h \
    processor/CollisionProcessor.h

FORMS += \
    widget/MainWidget.ui

win32{
        LIBS += -L$$OSG/lib -losg -losgGA -losgDB -losgUtil -losgViewer  -lOpenThreads -lopengl32
}

macx {
    LIBS += -L$$OSG/lib -losg -losgGA -losgDB -losgUtil -losgViewer -lOpenThreads
    LIBS += -L$$OSG/lib -lGLU -pthread
}

unix:!macx {
    LIBS += -L$$OSG/lib64 -losg -losgGA -losgDB -losgUtil -losgViewer -lOpenThreads
    LIBS += -L$$OSG/lib64 -lGLU -pthread
}


