#ifndef SCENEVIEWGL_H
#define SCENEVIEWGL_H
#include <memory>

#include <QWidget>
#include <QMatrix4x4>
#include <QOpenGLWidget>

class osgRenderer;
class SphereObject;
class SurfaceObject;
class SceneViewGL : public QOpenGLWidget
{
    Q_OBJECT


public:
    SceneViewGL(QWidget *parent = 0, SphereObject* simObj = nullptr, SurfaceObject* simSurf = nullptr);
    ~SceneViewGL();



protected:
    // OpenGL related methods
    virtual void 	initializeGL();
    virtual void 	paintGL();
    virtual void 	resizeGL(int w, int h);
    // Event Handling
    virtual bool    event(QEvent* event);

private:

    void checkOpenglContextVersion();
    void handleWheelZoom(QWheelEvent* wheelEvent);
    void handleMouseEvents(QEvent* event);


    void resetView();

private:

    // Qt OpenGL members
    QOpenGLContext* m_context = nullptr;

    // mouse related
    bool m_leftPressed  = false;
    bool m_rightPressed = false;
    QPointF m_mousePos;


    // render view dim
    int   m_width  = 0;
    int   m_height = 0;

    double m_zoom = 2.0;

    std::shared_ptr<osgRenderer> m_sceneRenderer = nullptr;

    bool m_initialized  = false;
};
#endif // SCENEVIEWGL_H
