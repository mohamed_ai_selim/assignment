// C++ headers
#include <cmath>
#include <string>
#include <vector>
#include <strstream>

// Qt headers go here
#include <QFile>
#include <QString>
#include <QWheelEvent>
#include <QApplication>

// GL headers
#include <QOpenGLContext>

#ifdef Q_OS_WIN
 #include <gl/GLU.h>
#else
 #include <GL/glu.h>
#endif


#include "simulation/Objects.h"
// renderer
#include "render/osgRenderer.h"
// class interface
#include "SceneViewGL.h"




SceneViewGL::SceneViewGL(QWidget *parent, SphereObject* simObj, SurfaceObject *simSurf)
    : QOpenGLWidget(parent)
{
    setFocusPolicy(Qt::ClickFocus);
    m_sceneRenderer.reset(new osgRenderer(simObj, simSurf));
}

SceneViewGL::~SceneViewGL()
{

}


void SceneViewGL::checkOpenglContextVersion()
{
    m_context   = QOpenGLContext::currentContext();
    // qDebug() <<"context pointer " <<context() << "context version: " << context()->format().majorVersion()<< "." << context()->format().minorVersion() ;
}

void SceneViewGL::initializeGL()
{
    m_context   = context();
    checkOpenglContextVersion();
    m_sceneRenderer->initialize();

    m_initialized = true;
}


void SceneViewGL::paintGL()
{

    if (m_sceneRenderer)
        m_sceneRenderer->render();
}

void SceneViewGL::resizeGL(int w, int h)
{
    makeCurrent();
    m_width = w;
    m_height = h;

    if (!m_sceneRenderer)
        return;

    m_sceneRenderer->setViewportSize(w, h);
}



bool SceneViewGL::event(QEvent *event)
{
    if (!m_sceneRenderer)
        return false;

    if (event->type() == QEvent::KeyPress){
        QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
        if (keyEvent->key() == Qt::Key_Space){
            m_sceneRenderer->home();
            update();
            return true;
        }
    }
    else if (event->type() == QEvent::Wheel)
    {
        QWheelEvent* wheelEvent = static_cast<QWheelEvent*>(event);

        handleWheelZoom(wheelEvent);
        m_sceneRenderer->setZoom(m_zoom);
        update();

        return true;
    }
    else if (event->type() == QEvent::MouseButtonPress ||
             event->type() == QEvent::MouseMove ||
             event->type() == QEvent::MouseButtonRelease||
             event->type() == QEvent::MouseButtonDblClick )
    {

        handleMouseEvents(event);

        update();
        return true;
    }
    else
        return QOpenGLWidget::event(event);


    return false;
}



void SceneViewGL::resetView()
{
    m_zoom = 2.0;
}

void SceneViewGL::handleMouseEvents(QEvent *event)
{
    QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
    QPoint point = mouseEvent->pos();
    if (event->type() == QEvent::MouseButtonPress)
    {
        m_mousePos = mouseEvent->pos();

        if (mouseEvent->button() == Qt::LeftButton) {
            m_leftPressed = true;
            m_rightPressed = false;
            m_sceneRenderer->mousePressEvent(point.x(), point.y(), 0);

        }
        else if (mouseEvent->button() == Qt::RightButton) {
            m_sceneRenderer->mousePressEvent(point.x(), point.y(), 1);
            m_leftPressed = false;
            m_rightPressed = true;
        }


    }
    else if (event->type() == QEvent::MouseMove)
    {
        m_mousePos = mouseEvent->pos();


        if (m_leftPressed)
        {
            this->setCursor(Qt::ClosedHandCursor);
            m_sceneRenderer->mouseDragEvent(point.x(), point.y(), 0);
        }
        else if (m_rightPressed){
            this->setCursor(Qt::ClosedHandCursor);
            m_sceneRenderer->mouseDragEvent(point.x(), point.y(), 1);
        }
    }
    else if (event->type() == QEvent::MouseButtonRelease){
        this->setCursor(Qt::ArrowCursor);
        if (m_leftPressed)
            m_sceneRenderer->mouseReleaseEvent(point.x(), point.y(), 0);
        else if (m_rightPressed)
            m_sceneRenderer->mouseReleaseEvent(point.x(), point.y(), 1);


        m_leftPressed  = false;
        m_rightPressed = false;
    }


    makeCurrent();
}


void SceneViewGL::handleWheelZoom(QWheelEvent* wheelEvent)
{
    double delta = wheelEvent->angleDelta().y()/(30.0);

    //qDebug() << "wheel delta" << delta;
    m_zoom = m_zoom + 0.12*delta;
    if (m_zoom > 80.0)
        m_zoom = 80.0;

    if (m_zoom < 0.1)
        m_zoom = 0.1;
}

