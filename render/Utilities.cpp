#include <set>
#include <vector>
#include <fstream>
#include <iostream>

#include <QFile>
#include <QTextStream>
#include <QXmlStreamReader>

#include <QDebug>

#include <QtXml/QDomNode>
#include <QtXml/QDomText>

#include "Utilities.h"

std::vector<std::string> split(const std::string &s, char delim)
{
    std::stringstream ss(s);
    std::string item;
    std::vector<std::string> elems;
    while (std::getline(ss, item, delim)) {
        //elems.push_back(item);
        elems.push_back(std::move(item)); // if C++11 (based on comment from @mchiasson)
    }
    return elems;
}



std::ostream& operator<<(std::ostream &os, const osg::BoundingBox& bb)
{
    return os << "bbox = ( " << bb.xMin() <<", "<< bb.yMin() <<", "<< bb.zMin() <<", "
              << bb.xMax() <<", "<< bb.yMax() <<", "<< bb.zMax() <<")";
}

std::ostream& operator<<(std::ostream &os, const osg::Vec3f& p)
{
    return os << "Vec3f ( " <<p.x() <<", " <<p.y() <<", "<<p.z()<<" )";
}

std::ostream& operator<<(std::ostream &os, const osg::Vec4f& p)
{
    return os << "Vec4f ( " <<p.r() <<", " <<p.g() <<", "<<p.b() <<", "<<p.a() <<" )";
}
std::ostream &operator<<(std::ostream &os, const osg::Quat &q)
{
     return os << "Qauternion ( " <<q.w() <<", " <<q.x() <<", "<<q.y() <<", "<<q.z() <<" )";
}

//      Euler's angles  expressed in       heading,     attitude,      bank
//      Euler's angles  expressed in       yam    ,     pitch,         roll

void toEulerianAngle(const osg::Quat& q, double& roll, double& pitch, double& yaw)
{
	double ysqr = q.y() * q.y();

	// roll (x-axis rotation)
	double t0 = +2.0 * (q.w() * q.x() + q.y() * q.z());
	double t1 = +1.0 - 2.0 * (q.x() * q.x() + ysqr);
	roll = std::atan2(t0, t1);

	// pitch (y-axis rotation)
	double t2 = +2.0 * (q.w() * q.y() - q.z() * q.x());
	t2 = t2 > 1.0 ? 1.0 : t2;
	t2 = t2 < -1.0 ? -1.0 : t2;
	pitch = std::asin(t2);

	// yaw (z-axis rotation)
	double t3 = +2.0 * (q.w() * q.z() + q.x() *q.y());
	double t4 = +1.0 - 2.0 * (ysqr + q.z() * q.z());
	yaw = std::atan2(t3, t4);
}



std::string getShaderFromResource(const QString &path)
{
    QFile file(path);
    file.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream in(&file);
    QString lines = in.readAll();

    return lines.toStdString();
}

osg::Vec3f stringToVec3(std::string &strVec)
{
    osg::Vec3f pt;

    std::vector<std::string> xy = split(strVec,',');

    if  (xy.size() > 2){
        pt.x() = std::stof (xy.at(0));
        pt.y() = std::stof (xy.at(1));
        pt.z() = std::stof (xy.at(2));
    }


    return pt;
}

std::vector<osg::Vec3f> readGeoCoordFromFile(std::string &&path)
{
    std::vector<osg::Vec3f> vec;
    std::ifstream in(path);
    int indx = 0;
    while(!in.eof())
    {
        osg::Vec3f pt;
        std::string lonStr, latStr, zStr, linStr;
        std::getline(in, linStr,'\n');
        std::vector<std::string> xy = split(linStr,',');
        //std::cout << "line read : " << linStr <<std::endl;
        //std::getline(in, zStr,',');
        if  (xy.size() > 2){
            pt.x() = std::stof (xy.at(0));
            pt.y() = std::stof (xy.at(1));
            pt.z() = std::stof (xy.at(2));
        }
        //pt.z() = std::stof (zStr);
        //if (!pt.isNaN() && ((indx % 10) == 0)/**/ )
            vec.push_back(pt);

        ++indx;

    }
    return vec;
}


QList<QStringList> xMLreadGeoCoordFromKML(const std::string &filename)
{
    QList<QStringList> result;
    QFile* xmlFile = new QFile(QString::fromStdString(filename));
    if (!xmlFile->open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() <<"Couldn't open xmlfile.xml to load settings for download";
        return result;
    }
    QXmlStreamReader* xmlReader = new QXmlStreamReader(xmlFile);


    //Parse the XML until we reach end of it
    while(!xmlReader->atEnd() && !xmlReader->hasError()) {
        // Read next element
        QXmlStreamReader::TokenType token = xmlReader->readNext();
        //If token is just StartDocument - go to next
        if(token == QXmlStreamReader::StartDocument) {
            continue;
        }
        //If token is StartElement - read it
        if(token == QXmlStreamReader::StartElement) {

            if(xmlReader->name() == "coordinates") {
                QString content = xmlReader->readElementText().trimmed();
                if (!content.isEmpty() && content.contains("\n\t\t\t\t\t")){
                    QStringList listofContent = content.split("\n\t\t\t\t\t");
                    if (!listofContent.isEmpty())
                        result.append(listofContent);
                }
            }
            else
                continue;
        }
    }

    if(xmlReader->hasError()) {

        qDebug() << "xml Parse Error file name = " << xmlFile->fileName();
        return result;
    }

    //close reader and flush file
    xmlReader->clear();
    xmlFile->close();
    delete xmlFile;
    return result;
}


std::vector<std::vector<osg::Vec3f> > processCoordStringList(const QList<QStringList> &coordList)
{
    auto stringToOsgVec3f = [](QString& coordStr) -> osg::Vec3f {
        osg::Vec3f resVec3f;

        QStringList listCoords = coordStr.split(",");
        if (listCoords.size() == 3){
            resVec3f.x() = listCoords[0].toFloat();
            resVec3f.y() = listCoords[1].toFloat();
            resVec3f.z() = listCoords[2].toFloat();
        }
        return resVec3f;
        };


    std::vector<std::vector<osg::Vec3f>> result;
    if (coordList.empty())
        return result;

    for (auto list: coordList){
      std::vector<osg::Vec3f> coordsVec;
      for (auto strCoord:list){

          osg::Vec3f vec;

          if (!strCoord.isEmpty()){
              vec = stringToOsgVec3f(strCoord);
             coordsVec.push_back(vec);
          }
      }
      result.push_back(coordsVec);
    }

    //std::cout << "no of osg vec groups " << result.size() << std::endl;
    return result;

}

void createMouseEvent(osgGA::GUIEventAdapter* event, int button, osgGA::GUIEventAdapter::EventType eventype, int &xs, int& ys, int w, int h)
{
    if (!event)
        return;

    event->setEventType(eventype);

    if (button == 0)
    {
      event->setButton(osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON);
      event->setButtonMask(osgGA::GUIEventAdapter::MIDDLE_MOUSE_BUTTON);
    }
    else
    {
      event->setButton(osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON);
      event->setButtonMask(osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON);
    }

    //event->setButtonMask(osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON);

    int x = xs;
    int y = ys;

    event->setWindowRectangle(0,0,w,h);
    float xmin = 0.0;
    float ymin = 0.0;
    float xmax = w;
    float ymax = h;
    event->setInputRange(xmin,ymin,xmax,ymax);
    event->setX(xs);
    event->setY(ys);
}



