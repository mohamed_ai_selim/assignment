#ifndef RENDERER_H
#define RENDERER_H

#include <iostream>

#include <QSize>
#include <QtGui/QOpenGLFunctions>
#include <QtGui/QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <osg/Camera>
#include <osg/ref_ptr>

#include <osg/Billboard>
#include <osg/BoundingBox>
#include <osg/MatrixTransform>

#include <osgUtil/CullVisitor>

#include <osgGA/OrbitManipulator>
#include <osgGA/DriveManipulator>
#include <osgGA/TerrainManipulator>
#include <osgGA/StandardManipulator>
#include <osgGA/SphericalManipulator>
#include <osgGA/TrackballManipulator>

#include <osgViewer/Viewer>

class SphereObject;
class SurfaceObject;
class osgRenderer : protected QOpenGLFunctions
{

public:
    osgRenderer(SphereObject* simSphere = nullptr, SurfaceObject* simSurface = nullptr);
    ~osgRenderer();

    // mouse related methods for redirecting mouse press, release and drag
    // to the camera manipulator for 3D manipulation
    void mousePressEvent(int x, int y, int button);
    void mouseDragEvent(int x, int y, int button);
    void mouseReleaseEvent(int x, int y, int button);

    void initialize();

    void render();

    void   setViewportSize(const int& width, const int& height);
    double getZoom();
    void   setZoom(double scale);
    void   home();

private:
    void initScene();
    void initGroundGeometry();
    void initSurfaceGeometry();

    void setupScene();
    void setupCameraManipulator();

private:
    QSize mSize;
    osg::Vec2 mBottomLeft, mTopRight;
    // shere animated object
    SphereObject* m_sphere = nullptr;
    SurfaceObject* m_surface = nullptr;
    // osg scene objects
    osg::ref_ptr<osg::Group> mSceneRoot;
    osg::ref_ptr<osg::Camera> mCamera;
    osg::ref_ptr<osgViewer::Viewer> mViewer;
    osg::ref_ptr<osgGA::TrackballManipulator> mCameraManip;
    osg::ref_ptr<osg::Geode> m_surfaceGeode;
    osg::ref_ptr<osg::Geode> m_groundGeode;
    // embedded osg viewer
    osgViewer::GraphicsWindowEmbedded* mEmbeddedWin;

    double mZoom = 1.0;
    double mHomeDistance = 1.0;


};


#endif // RENDERER_H
