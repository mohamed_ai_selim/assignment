#include <random>
#include <string>
#include <sstream>
#include <iostream>

#include <osg/Math>
#include <osg/Array>
#include <osg/Matrix>

#include <osg/ValueObject>

#include <osg/Image>
#include <osg/Shader>
#include <osg/CullFace>
#include <osg/BlendFunc>
#include <osg/Texture2D>


#include <osg/Hint>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/ShapeDrawable>

#include <osgUtil/RenderBin>
#include <osgUtil/Tessellator>
#include <osgUtil/TriStripVisitor>
#include <osgUtil/SmoothingVisitor>

#include <osg/Depth>
#include <osg/Light>
#include <osg/Point>
#include <osg/Material>
#include <osg/LineWidth>
#include <osg/PolygonMode>

#include <osg/AutoTransform>

#include <osg/Viewport>

#include <osgDB/ReadFile>
#include <osgGA/EventQueue>
#include <osgGA/GUIEventAdapter>


//
#include "Utilities.h"
#include "simulation/Objects.h"
// class header
#include "osgRenderer.h"


// update callback functor that's responsible
// for updating the ball position each frame
class TransformCB : public osg::NodeCallback
{
public:
    TransformCB(SphereObject* sphere)
    {
        _sphere = sphere;
    }
    virtual void operator()( osg::Node* node, osg::NodeVisitor* nv )
    {
        osg::MatrixTransform* trans = dynamic_cast<osg::MatrixTransform*>(node);
        if (!trans)
            return;

        if (!_sphere)
            return;
        //The OSG's loaders and camera manipulators all default to Y north, X
        //east, Z up, just like your data...but.. the .obj loader automatically
        //does a rotate to compensate for the fact that most .obj data comes in
        //with Y up, X to the right, Z out.
        osg::Vec3d p = _sphere->getPosition();
        //trans->getMatrix().getTrans();

        trans->setMatrix(osg::Matrix::translate(p));
        traverse(node, nv);

    }

private:
    SphereObject* _sphere = nullptr;
};


osg::Drawable* createAxis(const osg::Vec3& corner,const osg::Vec3& xdir,const osg::Vec3& ydir, const osg::Vec3& zdir)
{
    // set up the Geometry.
    osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;

    osg::Vec3Array* coords = new osg::Vec3Array(6);
    (*coords)[0] = corner;
    (*coords)[1] = corner + xdir;
    (*coords)[2] = corner;
    (*coords)[3] = corner + ydir;
    (*coords)[4] = corner;
    (*coords)[5] = corner + zdir;

    geom->setVertexArray(coords);

    osg::Vec4 x_color(0.0f,0.0f,1.0f,1.0f);
    osg::Vec4 y_color(1.0f,0.0f,0.0f,1.0f);
    osg::Vec4 z_color(0.0f,1.0f,0.0f,1.0f);

    osg::Vec4Array* color = new osg::Vec4Array(6);
    (*color)[0] = x_color;
    (*color)[1] = x_color;
    (*color)[2] = y_color;
    (*color)[3] = y_color;
    (*color)[4] = z_color;
    (*color)[5] = z_color;

    geom->setColorArray(color, osg::Array::BIND_PER_VERTEX);

    geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINES,0,6));

    osg::StateSet* stateset = geom->getOrCreateStateSet();
    osg::LineWidth* linewidth = new osg::LineWidth();
    linewidth->setWidth(3.0f);
    stateset->setAttributeAndModes(linewidth,osg::StateAttribute::ON);
    stateset->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

    return geom.release();
}

osgRenderer::osgRenderer(SphereObject *simSphere, SurfaceObject *simSurface)
{

    mSize = QSize(755, 400);
    m_sphere = simSphere;
    m_surface = simSurface;
}

osgRenderer::~osgRenderer()
{
}

void osgRenderer::initialize()
{
    initScene();
    initGroundGeometry();
    initSurfaceGeometry();
    setupScene();
    setupCameraManipulator();
}

void osgRenderer::render()
{
    //glUseProgram(0);
    mViewer->frame();
}

void osgRenderer::initScene()
{
    {
        // virtual function
        initializeOpenGLFunctions();
        GLint texture_units, combined_tex_units;
        glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &texture_units);
        glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &combined_tex_units);

        const GLubyte* renderer = glGetString(GL_RENDERER);
        std::string sren = reinterpret_cast< char const * >(renderer);
        qDebug() << QString::fromStdString(sren);
        //std::cout << "GL_MAX_TEXTURE_IMAGE_UNITS          = " << texture_units << std::endl;
        //std::cout << "GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS = " << combined_tex_units << std::endl;
    }

    mViewer = new osgViewer::Viewer;
    // setup as embedded
    mEmbeddedWin = mViewer->setUpViewerAsEmbeddedInWindow(0, 0, mSize.width(), mSize.height());

    mSceneRoot = new osg::Group;
    // single threaded rendering
    mViewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);
    mCamera = mViewer->getCamera();

    mCamera->setViewport(0, 0, mSize.width(), mSize.height());
    mCamera->setClearColor(osg::Vec4f(0.0, 0.0, 0.0, 0.0));
    mCamera->setClearMask( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    mCameraManip = new osgGA::TrackballManipulator;

    mViewer->setCameraManipulator(mCameraManip.get());
    mViewer->setSceneData(mSceneRoot);

}

void osgRenderer::initGroundGeometry()
{

    m_groundGeode = new osg::Geode;
    osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
    geom->setUseVertexBufferObjects(true);

    osg::Vec3Array* vx = new osg::Vec3Array;
    osg::ref_ptr<osg::Vec4Array> cl = new osg::Vec4Array;
    cl->push_back(osg::Vec4f(0.87f, 0.88f, 0.87f, 0.99f));
    osg::Vec3Array *nArr= new osg::Vec3Array;
    nArr->push_back(osg::Vec3f(0.f, 0.f , 1.f));
    geom->setNormalArray(nArr);
    geom->setNormalBinding(osg::Geometry::BIND_OVERALL);
    geom->setColorArray(cl);
    geom->setColorBinding(osg::Geometry::BIND_OVERALL);
    geom->setVertexArray(vx);


    float dim = 300.0f;
    float step = 30.0f;
     std::size_t lastSize = 0;
    // building up the vertex array (to be added later on as VBO)
    for(float x = -dim; x <= dim; x = x + step)
    {
        int j = 0;
        for (float y = -dim; y <= dim; y = y + step)
        {
            float z =  0.0;
            vx->push_back(osg::Vec3f(x, y, z));

        }
        lastSize = vx->size();
    }
    geom->addPrimitiveSet(new osg::DrawArrays( osg::PrimitiveSet::POINTS, /*lastSize*/0, vx->size()/* - lastSize*/) );

    m_groundGeode->addDrawable(geom);
    m_groundGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
}

void osgRenderer::setupScene()
{
    float radius = static_cast<float>( m_sphere->getRadius());
    osg::ref_ptr<TransformCB> cb = new TransformCB(m_sphere);


    osg::ref_ptr<osg::MatrixTransform> transformGroup = new osg::MatrixTransform;

    osg::Vec3d initPos = m_sphere->getInitialPosition();
    transformGroup->setMatrix( osg::Matrix::translate(initPos) );

    osg::ShapeDrawable* shapeSph = new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(0.0, 0.0, 0.0), radius));
    shapeSph->setColor(osg::Vec4(1.0, 0.0, 1.0, 0.5));

    osg::Geode* geodeShape = new osg::Geode;

    geodeShape->addDrawable(shapeSph);

    transformGroup->addChild(geodeShape);
    transformGroup->setUpdateCallback(cb);
    // building up the scene graph
    mSceneRoot->addChild(m_groundGeode);
    mSceneRoot->addChild(createAxis( osg::Vec3f(0.0f, 0.0f, 0.0f), osg::Vec3f(100.0f, 0.0f, 0.0f),
                                     osg::Vec3f(0.0f, 100.0f, 0.0f), osg::Vec3f(0.0f, 0.0f, 100.0f)));
    mSceneRoot->addChild(transformGroup);
    mSceneRoot->addChild(m_surfaceGeode);

}

void osgRenderer::setupCameraManipulator()
{
    double bsRadius = static_cast<double>(mSceneRoot->getBound().radius());
    osg::Vec3d lookDir = osg::Y_AXIS * bsRadius;
    osg::Vec3d up(0.0, 0.0, 1.0);
    osg::Vec3d center = osg::Vec3d();//mSceneRoot->getBound().center();
    //The OSG's loaders and camera manipulators all default to Y north, X
    //east, Z up, just like your data...but.. the .obj loader automatically
    //does a rotate to compensate for the fact that most .obj data comes in
    //with Y up, X to the right, Z out.

    osg::Vec3d eye = osg::Vec3d(0.0, -3.0, 0.0);//center -  (lookDir*radius*5.0);

    double ar = mSize.width()/ mSize.height();

    mCamera->setProjectionMatrixAsPerspective( 50.0, ar, 0.1, 1000.0 );
    mCamera->setViewMatrixAsLookAt(eye, lookDir, up );
    mViewer->home();

    mCameraManip->setHomePosition(center, lookDir, up, true);

    mHomeDistance =  mCameraManip->getDistance();
    setZoom(mZoom);

}


// handling viewport size when window size is changed
// recalculating aspect ration and feeding it to
// camera again.
void osgRenderer::setViewportSize(const int& width, const int& height)
{
    mSize = QSize(width, height);
    if (!mCamera.valid())
        return;

    double ar = 1.0;
    ar =(((double)mSize.width())/((double)mSize.height()));

    if (mCamera)
    {
        double fov, art, np, fp;
        mCamera->getProjectionMatrixAsPerspective( fov, art, np, fp );
        mCamera->setProjectionMatrixAsPerspective( fov, ar,  np, fp );
        mCamera->setViewport(0, 0, mSize.width(), mSize.height());
    }
}

qreal osgRenderer::getZoom()
{
    return mZoom;
}

void osgRenderer::setZoom(double scale)
{
    mZoom = scale;

    double dist = mHomeDistance / mZoom;
    mCameraManip->setDistance(dist);
}

void osgRenderer::home()
{
    if (mViewer)
        mViewer->home();
}


//TODO: needs to be continued still an initial
// endeavour for rendering 3D surface from parametric equation
// since this module is not builtin in osg, and needs to be implemented
void osgRenderer::initSurfaceGeometry()
{
    m_surfaceGeode = new osg::Geode;
    osg::ref_ptr<osg::Vec4Array> cl = new osg::Vec4Array;
    cl->push_back(osg::Vec4f(0.7f, 0.7f, 0.7f, 0.7f));
    osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
    osg::Vec3Array* vx = new osg::Vec3Array;
    geom->setColorArray(cl);
    geom->setColorBinding(osg::Geometry::BIND_OVERALL);

    osg::Vec3Array *nArr= new osg::Vec3Array;
    //nArr->push_back(osg::Vec3f(0.f, 0.f , 1.f));

    geom->setNormalArray(nArr);
    geom->setNormalBinding(osg::Geometry::BIND_PER_PRIMITIVE_SET);
    geom->setUseVertexBufferObjects(true);

    float step = 7.f;
    float dim  = 250.0f;


    osg::ref_ptr<osg::DrawElementsUInt> element_indices = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS);



    int i = 0;
    int l = 0;
    // building up the vertex array (to be added later on as VBO)
    for(float x = -dim; x <= dim; x = x + step)
    {
        int j = 0;
        for (float y = -dim; y <= dim; y = y + step)
        {
            //qDebug() << j;
            //Y = 0.3*SIN(3*SQRT((X-5)2+(Z-5)2)) + 0.5*COS(X+Z)
            //float z =    3.0f*std::sin(0.1f*std::sqrt( std::pow((x - 5.0f), 2.0f) + std::pow((y - 5.0f), 2.0f)) )
             //          +  5.0f*std::cos( x + y );
            float z = m_surface->computeZ(x, y);
            vx->push_back(osg::Vec3f(x, y, z));

            ++j;
            l = std::max(l, j);
        }
        i++;
    }

    //float t = std::floor(((dim*2) /step ) +1 );
    i = 0;
    unsigned int ind = 0;
    // setting up the draw element, i.e. determining
    // draw element from vertix array that was previously
    // built. drawelement should build up how the verteces are arranged
    // and in which layout i.e. gl_triangle_fan , gl_triangle_strips...etc.
    for(float x = -dim; x <= dim; x = x + step)
    {
        int j = 0;
        for (float y = -dim; y <= dim; y = y + step)
        {
            unsigned int offset = l;
            // insuring that we are not running on the vertices on the edges
            if (i <= (l - static_cast<int>(step)))
            if (j <= (l - static_cast<int>(step))){
                unsigned int p = ind + offset;
                element_indices->push_back(p);
                element_indices->push_back(p+1);
                element_indices->push_back(ind+1);
                element_indices->push_back(ind);
                // calc normal for every primitive i.e. quads
                osg::Vec3f one, two, n = osg::Vec3f(0.f, 0.f , 1.f);
                if (( (ind+1) < vx->size()) && ( (p+1) < vx->size())){
                    one = vx->at(p) - vx->at(ind);
                    two = vx->at(ind+1) - vx->at(ind);
                    //n = one ^ two;
                    //n.normalize();
                }
                nArr->push_back(n);
            }
            ++j;
            ++ind;
        }
        i++;
    }

    int sizze = element_indices->size()/4;
    int sizev = vx->size();

    geom->addPrimitiveSet(element_indices);
    geom->setVertexArray(vx);
    // tesselating the geometry from quads to triangles
    // hopefully it would also tesselllate the normal with
    // correct smoothing crease angle.
    osgUtil::SmoothingVisitor snv;
    snv.setCreaseAngle(osg::PI);
    // tessellator processor
    osgUtil::Tessellator tessellator;
    // tristrip for converting from Quads to Triagles
    osgUtil::TriStripVisitor tristrip;

    tessellator.retessellatePolygons( *geom );
    tristrip.stripify(*geom);
    snv.smooth(*geom, osg::PI);


    m_surfaceGeode->addDrawable(geom);

    //NOTE: for debugggin and inspecting mesh surface with out filling and lighting
    //osg::ref_ptr<osg::PolygonMode> plymode = new osg::PolygonMode;
    //plymode->setMode(osg::PolygonMode::FRONT, osg::PolygonMode::LINE);
    //plymode->setMode(osg::PolygonMode::BACK, osg::PolygonMode::LINE);
    //geom->getOrCreateStateSet()->setAttributeAndModes(plymode.get(), osg::StateAttribute::ON);
}

void osgRenderer::mousePressEvent(int x, int y, int button)
{
    osgGA::GUIEventAdapter* event = new osgGA::GUIEventAdapter;
    createMouseEvent(event, button, osgGA::GUIEventAdapter::PUSH, x, y, mSize.width(), mSize.height());
    mViewer->getEventQueue()->addEvent(event);
}


void osgRenderer::mouseDragEvent(int x, int y, int button){
    osgGA::GUIEventAdapter* event = new osgGA::GUIEventAdapter;
    createMouseEvent(event, button, osgGA::GUIEventAdapter::DRAG, x, y, mSize.width(), mSize.height());
    mViewer->getEventQueue()->addEvent(event);
}

void osgRenderer::mouseReleaseEvent(int x, int y, int button)
{
    osgGA::GUIEventAdapter* event = new osgGA::GUIEventAdapter;
    createMouseEvent(event, button, osgGA::GUIEventAdapter::RELEASE, x, y, mSize.width(), mSize.height());
    mViewer->getEventQueue()->addEvent(event);
}





