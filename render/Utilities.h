#ifndef UTILITIES_H
#define UTILITIES_H

// C++
#include <string>
#include <sstream>

// osg, osgGA
#include <osg/Quat>
#include <osg/Vec3f>
#include <osg/Vec3d>
#include <osg/Vec4f>
#include <osg/Matrixd>
#include <osg/BoundingBox>

#include <osgGA/GUIEventAdapter>

//Qt
#include <QString>
std::ostream& operator<<(std::ostream &os, const osg::BoundingBox& bb);
std::ostream& operator<<(std::ostream &os, const osg::Vec3f& p);
std::ostream& operator<<(std::ostream &os, const osg::Vec4f& p);
std::ostream& operator<<(std::ostream &os, const osg::Quat& q);

void createMouseEvent(osgGA::GUIEventAdapter* event, int button, osgGA::GUIEventAdapter::EventType eventype, int &xs, int& ys, int w, int h);

//      Euler's angles  expressed in       heading,     attitude,      bank
//                                         roll,        pitch,         yaw
void toEulerianAngle(const osg::Quat& q, double& roll, double& pitch, double& yaw);

const std::string imgsPath("/home/mselim/Development/Projects/images/");
osg::Vec3f stringToVec3(std::string &strVec);
std::vector<std::string> split(const std::string &s, char delim) ;
std::string getShaderFromResource(const QString &path);
std::vector<osg::Vec3f> readGeoCoordFromFile(std::string &&path);

QList<QStringList> xMLreadGeoCoordFromKML(const std::string &filename);
std::vector<std::vector<osg::Vec3f>> processCoordStringList(const QList<QStringList>& coordList);
#endif // UTILITIES_H
