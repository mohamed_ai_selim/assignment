#include <QApplication>

#include "widget/MainWidget.h"

#include "core/CoreApplication.h"

int main(int argc, char *argv[])
{
    CoreApplication a(argc, argv);
    MainWidget w;
    w.show();

    return a.exec();
}
