#ifndef COLLISIONPROCESSOR_H
#define COLLISIONPROCESSOR_H

// forward declarations
class SphereObject;
class GroundObject;

//TODO: needs to be implemented

// class for computing collision detection
// for sphere(Object) and ground
class CollisionProcessor
{
public:
    CollisionProcessor(SphereObject* sphere, GroundObject* ground);
    ~CollisionProcessor();
    void checkCollision();
private:

private:
    //TODO: surface object instead of ground
    GroundObject* m_ground = nullptr;
    SphereObject* m_sphere = nullptr;
};
#endif //COLLISIONPROCESSOR_H
