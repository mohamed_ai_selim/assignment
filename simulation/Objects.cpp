#include "Objects.h"

SphereObject::SphereObject(const osg::Vec3d &initial_pos, const osg::Vec3d &initial_vel, const double& rad)
{
    m_initialPosition = initial_pos;
    m_position = initial_pos;
    m_initialvelocity = initial_vel;
    m_radius = rad;
}

SphereObject::~SphereObject()
{

}

osg::Vec3d SphereObject::getAcceleration()
{
    return m_acceleration;
}

osg::Vec3d SphereObject::getVelocity()
{
    return m_velocity;
}

osg::Vec3d SphereObject::getPosition()
{
    return m_position;
}

double SphereObject::getRadius()
{
    return m_radius;
}

osg::Vec3d SphereObject::getInitialVelocity()
{
    return m_initialvelocity;
}

osg::Vec3d SphereObject::getInitialPosition()
{
    return m_initialPosition;
}

void SphereObject::setInitialPosition(const osg::Vec3d &p)
{
    m_initialPosition = p;
}

void SphereObject::setInitialVelocity(const osg::Vec3d &v)
{
    m_initialvelocity = v;
}

void SphereObject::setRadius(const double &rad)
{
    m_radius = rad;
}

void SphereObject::setAcceleration(const osg::Vec3d &a)
{
    m_acceleration = a;
}

void SphereObject::setVelocity(osg::Vec3d &v)
{
    m_velocity = v;
}

void SphereObject::setPosition(osg::Vec3d &p)
{
    m_position = p;
}

GroundObject::GroundObject(osg::Vec3d pos)
{
    position = pos;
}

float SurfaceObject::computeZ(const float &x, const float &y)
{
    float z =    3.0f*std::sin(0.1f*std::sqrt( std::pow((x - 5.0f), 2.0f) + std::pow((y - 5.0f), 2.0f)) )
               +  5.0f*std::cos( x + y );
    return z;
}
