#ifndef SIMULATIONCONTROLLER_H
#define SIMULATIONCONTROLLER_H
#include <memory>
#include <vector>
#include <chrono>

// TODO: the logic for collision detection
// should be moved to specific class i.e. Collision Processer

// forward declaration
class SphereObject;
class GroundObject;
class SurfaceObject;
// simulation controller class
class SimulationController
{
public:
    SimulationController();
    ~SimulationController();

    SphereObject* getSphere();
    SurfaceObject* getSurface();
    void advance();
private:
    void updateTimeElapsed();
    void initializeSimulation();
    void computeSimulationFrame();


    void resetSimulationTime();

private:
    // scene objects
    std::shared_ptr<SphereObject> m_sphere = nullptr;
    std::shared_ptr<GroundObject> m_ground = nullptr;
    std::shared_ptr<SurfaceObject> m_surface = nullptr;
    bool m_firstTime = true;
    // simulation time variables
    double m_realDeltaTime = 0.0;
    std::int64_t m_deltaTime = 0;
    std::chrono::time_point<std::chrono::high_resolution_clock> m_initialTime;
    std::chrono::time_point<std::chrono::high_resolution_clock> m_previousTime;
};
#endif //SIMULATIONCONTROLLER_H
