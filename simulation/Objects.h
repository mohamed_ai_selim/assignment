#ifndef SPHEREOBJECT_H
#define SPHEREOBJECT_H
#include <osg/Vec3d>

class GroundObject{
public:
    GroundObject(osg::Vec3d pos);
    ~GroundObject() = default;
    osg::Vec3d position;
};

class SurfaceObject{
public:
    SurfaceObject()= default;
    ~SurfaceObject() = default;
    float computeZ(const float& x, const float& y);
};

class SphereObject
{
public:
    SphereObject(const osg::Vec3d& initial_pos, const osg::Vec3d& initial_vel, const double& rad);
    ~SphereObject();
    void clear();

    osg::Vec3d getAcceleration();
    osg::Vec3d getVelocity();
    osg::Vec3d getPosition();
    double getRadius();

    osg::Vec3d getInitialVelocity();
    osg::Vec3d getInitialPosition();

    void setInitialPosition(const osg::Vec3d& p);
    void setInitialVelocity(const osg::Vec3d& v);

    void setRadius(const double& rad);
    void setAcceleration(const osg::Vec3d &a);
    void setVelocity(osg::Vec3d& v);
    void setPosition(osg::Vec3d& p);


private:
    double m_radius = 1.0;
    osg::Vec3d m_initialPosition;
    osg::Vec3d m_acceleration;
    osg::Vec3d m_position;
    osg::Vec3d m_initialvelocity;
    osg::Vec3d m_velocity;
};

#endif // SPHEREOBJECT_H
