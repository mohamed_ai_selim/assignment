#include <osg/Vec3f>
#include <osg/Vec3d>

#include "Objects.h"

// class header
#include "SimulationController.h"

const double damping_factor = 0.44;
// acceleration vector
const osg::Vec3d gravity = osg::Vec3d(0.0, 0.0, -9.8);


SimulationController::SimulationController()
{
    initializeSimulation();
}

SimulationController::~SimulationController()
{
    m_ground.reset();
    m_surface.reset();
    m_sphere.reset();
}

void SimulationController::updateTimeElapsed()
{
    const auto thistime = std::chrono::high_resolution_clock::now();
    m_deltaTime =  std::chrono::duration_cast<std::chrono::milliseconds>(thistime  - m_initialTime).count();
    m_realDeltaTime = m_deltaTime / 1000.0;
}

SphereObject *SimulationController::getSphere()
{
    return m_sphere.get();
}

SurfaceObject *SimulationController::getSurface()
{
    return m_surface.get();
}

void SimulationController::advance()
{
    if (m_firstTime){
        m_firstTime = false;
        m_initialTime = std::chrono::high_resolution_clock::now();
    }
    updateTimeElapsed();
    computeSimulationFrame();
}

void SimulationController::initializeSimulation()
{

    m_sphere.reset(new SphereObject(osg::Vec3d(5.0, 0.0, 120.0), osg::Vec3d(5.0, 5, 0.0), 3.0));
    m_sphere->setAcceleration(gravity);
    m_surface.reset(new SurfaceObject);
    m_ground.reset(new GroundObject(osg::Vec3d(0, 0, 0)));

}

void SimulationController::computeSimulationFrame()
{
    double delta_t = m_realDeltaTime;

    osg::Vec3d init_pos = m_sphere->getInitialPosition();
    osg::Vec3d init_vel = m_sphere->getInitialVelocity();
    osg::Vec3d acc      = m_sphere->getAcceleration();


    osg::Vec3d vel =  acc*delta_t /*- init_vel*/;

    vel.x() = vel.x()*0.8;
    vel.y() = vel.y()*0.8;
    m_sphere->setVelocity(vel);

    osg::Vec3d delta_r = init_pos
                         + init_vel * delta_t
                         + acc * 0.5 * delta_t * delta_t ;
    // TODO: this is a basic naiive implementation, it needs to be enhanced to take into consideration the
    // exact position on the surface and collision point with respect to the physical model, i.e. rebound v should be
    // in the normal to the contact point in 3d.
    // check collision
    // if hits/passes the ground
    // sphere init velocity is set to vel.z() * -1 * damping factor
    float sz = m_surface->computeZ(delta_r.x(), delta_r.y());
     //float sz =  m_ground->position.z();
    if (delta_r.z() - m_sphere->getRadius() <=  sz)
    {
        // init_pos =  osg::Vec3d(delta_r.x(), delta_r.y(), m_ground->position.z() + m_sphere->getRadius() + 0.002);
        init_pos =  osg::Vec3d(delta_r.x(), delta_r.y(), sz + m_sphere->getRadius() + 0.002);
        m_sphere->setInitialPosition(init_pos);

        init_vel.z() = vel.z()*-1.0*damping_factor;
        init_vel.x() = init_vel.x()*0.99;
        init_vel.y() = init_vel.y()*0.99;

        m_sphere->setInitialVelocity(init_vel);

        resetSimulationTime();
    }
    m_sphere->setPosition(delta_r);
}

void SimulationController::resetSimulationTime()
{
     m_firstTime = true;
}


