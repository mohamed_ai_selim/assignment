# assignment readme to be done

-- the application is based on Qt 5.7 and OSG 3.4.0

-- the application is cross platform Windows, Linux and Mac OS X (not tested yet)

-- the simulation physics is a bit naiive, lacks the exact physical model on the collision detection and
   the correct physical model simulation, i.e. collision detection is perfomed on the Z-Axis only poisition of the sphere
   the rebound based on mv ' = mv at the contact point of the collision should rather be computer on the normal to the surface
   at the contact point.
   
-- the application performs  basic rendering of the 3d-surface, a better wokr should be done to enhance the rendering

-- communication module based on event/publish/subscribtion pattern is present but not time to implement it.
   i.e. the physics simulation controller should communicate timer stop event when the ball has reached Zeno's phenomenon 
   and it's posiition has reached infinitesimal displacements.  
