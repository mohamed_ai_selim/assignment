// C++
#include <chrono>

// Qt headers
#include <QDebug>
#include <QTimer>
#include <QMessageBox>

#include <QOpenGLContext>

//  global scope Headers
#include "scopes/GlobalScope.h"

// events
#include "events/EventQueue.h"
#include "events/EventManager.h"
#include "events/EventPublisher.h"
#include "events/EventDispatcher.h"

#include "CoreApplication.h"




CoreApplication::CoreApplication(int &argc, char **argv): QApplication(argc, argv)
{
    initLibraryPaths();
    initialize();
    initializeGLContext();
}

bool CoreApplication::notify(QObject *receiver, QEvent *event)
{
   // qDebug() << "notify is called";
    try
    {
        if (checkEventQueue()){
            m_eventDispatcher->processEventQueue(m_eventQueue);
        }

        return QApplication::notify(receiver, event);
    }
    catch(std::exception& e)
    {
        qDebug() << "Exception thrown:" << QString::fromStdString(e.what());
        QMessageBox::critical(0, QString("error"), QString(e.what()));
    }
    catch(...)
    {
        qDebug() << "Unfortunately some thing bad has happened";
    }
    return false;
}

void CoreApplication::customEvent(QEvent *event)
{

}

void CoreApplication::initLibraryPaths()
{
    addLibraryPath("\\");
    addLibraryPath("./plugins/");
}

void CoreApplication::initialize()
{
    m_globalScope = scopes::GlobalScope::getInstance(this);

    m_eventQueue.reset(new EventQueue);

    m_eventManager    = EventManager::getInstance();
    m_eventDispatcher = EventDispatcher::getInstance();
    m_eventPublisher  = EventPublisher::getInstance(m_eventQueue);

    m_globalScope->setEventManager(m_eventManager);
    m_globalScope->setEventPublisher(m_eventPublisher);

    // animation timer related initialization
    using framemmseconds = std::chrono::duration<int, std::ratio<1, 60>>;
    framemmseconds fps(1);

    m_simulationTimer.reset(new QTimer);
    m_simulationTimer->setTimerType(Qt::PreciseTimer);
    m_simulationTimer->setInterval(16);

    m_globalScope->setAnimationTimer(m_simulationTimer);
}

void CoreApplication::initializeGLContext()
{
    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    format.setStencilBufferSize(8);
    format.setSamples(4);
    format.setVersion(2, 1);
    format.setProfile(QSurfaceFormat::CoreProfile);

    QSurfaceFormat::setDefaultFormat(format);
}

bool CoreApplication::checkEventQueue()
{
    if (!m_eventQueue)
        return false;
    else
        return m_eventQueue->status();
}
