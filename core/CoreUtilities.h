#ifndef COREUTILITIES_H
#define COREUTILITIES_H

#include <chrono>
#include <iostream>
#include <functional>

namespace coreTest {

    template <class FN, class TC, typename... TArgs>
    void perf_run_micro_seconds(FN& mf, TC* obj, TArgs...args)
    {
        auto fn = std::bind(mf, obj, args...);

        const auto start = std::chrono::high_resolution_clock::now();
        fn();
        const auto finish = std::chrono::high_resolution_clock::now();
        const auto duration = std::chrono::duration_cast<std::chrono::microseconds>
                              (finish - start).count();
        std::cout << "the function call duration is " << duration << " micro second" << std::endl;
    }

    template <class FN, class TC, typename... TArgs>
    void perf_run_milli_seconds(FN& mf, TC* obj, TArgs...args)
    {
        auto fn = std::bind(mf, obj, args...);

        const auto start = std::chrono::high_resolution_clock::now();
        fn();
        const auto finish = std::chrono::high_resolution_clock::now();
        const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>
                              (finish - start).count();
        std::cout << "the function call duration is " << duration << " milli second" << std::endl;
    }

}

#endif // COREUTILITIES_H
