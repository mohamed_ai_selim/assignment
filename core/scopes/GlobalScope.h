#ifndef GLOBALSCOPE_H
#define GLOBALSCOPE_H

#include <memory>

class QTimer;
class EventQueue;
class EventManager;
class EventPublisher;
class CoreApplication;

namespace scopes {


    class GlobalScope
    {
    public:
        static std::shared_ptr<GlobalScope> getInstance(CoreApplication* parent = nullptr);

        std::shared_ptr<EventManager> getEventManager();
        std::shared_ptr<EventPublisher> getEventPublisher();

        std::shared_ptr<QTimer> getAnimationTimer();

        void setEventManager(std::shared_ptr<EventManager> eventManager);
        void setAnimationTimer(std::shared_ptr<QTimer> timer);
        void setEventPublisher(std::shared_ptr<EventPublisher> eventPublisher);

        ~GlobalScope();
    private:
        GlobalScope();
        std::shared_ptr<QTimer>          m_animationTimer  = nullptr;
        std::shared_ptr<EventManager>    m_eventManager    = nullptr;
        std::shared_ptr<EventPublisher>  m_eventPublisher  = nullptr;
        // singleton instance
        static std::shared_ptr<GlobalScope> m_instance;
    };
}

#endif // GLOBALSCOPE_H
