#include "core/events/EventQueue.h"
#include "core/events/EventManager.h"
#include "core/events/EventPublisher.h"

#include "core/scopes/GlobalScope.h"


std::shared_ptr<scopes::GlobalScope> scopes::GlobalScope::m_instance = nullptr;

std::shared_ptr<scopes::GlobalScope> scopes::GlobalScope::getInstance(CoreApplication* parent)
{
    if (m_instance == nullptr)
        m_instance.reset(new GlobalScope);
    return m_instance;
}

std::shared_ptr<EventManager> scopes::GlobalScope::getEventManager()
{
    return m_eventManager;
}

std::shared_ptr<EventPublisher> scopes::GlobalScope::getEventPublisher()
{
    return m_eventPublisher;
}

std::shared_ptr<QTimer> scopes::GlobalScope::getAnimationTimer()
{
    return m_animationTimer;
}

void scopes::GlobalScope::setEventManager(std::shared_ptr<EventManager> eventManager)
{
    m_eventManager   = eventManager;

}

void scopes::GlobalScope::setAnimationTimer(std::shared_ptr<QTimer> timer)
{
    m_animationTimer = timer;
}

void scopes::GlobalScope::setEventPublisher(std::shared_ptr<EventPublisher> eventPublisher)
{
    m_eventPublisher = eventPublisher;
}


scopes::GlobalScope::GlobalScope()
{

}

scopes::GlobalScope::~GlobalScope()
{

}

