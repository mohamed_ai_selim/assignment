#ifndef COREAPPLICATION_H
#define COREAPPLICATION_H
// C++ headers
#include <memory>
// Qt headers
#include <QApplication>

// forward declaration
namespace scopes{
    class GlobalScope;
}

class QTimer;
class EventQueue;
class EventManager;
class EventPublisher;
class EventDispatcher;

class CoreApplication: public QApplication{
    Q_OBJECT

public:
    CoreApplication(int& , char **);
    virtual bool notify(QObject * receiver, QEvent * event);
protected:
    void customEvent(QEvent * event);

private:
    void initLibraryPaths();
    void initialize();
    void initializeGLContext();
    bool checkEventQueue();
    void handleTimer();

    std::shared_ptr<QTimer> m_simulationTimer;
    std::shared_ptr<EventQueue> m_eventQueue;
    std::shared_ptr<scopes::GlobalScope> m_globalScope;
    std::shared_ptr<EventManager> m_eventManager;
    std::shared_ptr<EventDispatcher> m_eventDispatcher;
    std::shared_ptr<EventPublisher>  m_eventPublisher;
};

#endif //COREAPPLICATION_H
