#ifndef UPDATEEVENT_H
#define UPDATEEVENT_H

#include "Event.h"

class UpdateEvent: public Event
{
public:
    UpdateEvent(Event::Type type = Event::Type::TEST);
    virtual ~UpdateEvent();
    void setValue(float val);
    float getValue() const;
protected:
    float m_value = 0.0f;
};

#endif // UPDATEEVENT_H
