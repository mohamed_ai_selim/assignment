#include "Event.h"
#include "EventQueue.h"

#include "EventPublisher.h"


std::shared_ptr<EventPublisher> EventPublisher::m_instance = nullptr;

std::shared_ptr<EventPublisher> EventPublisher::getInstance(std::shared_ptr<EventQueue> queue)
{
    if (m_instance == nullptr){
        if (queue)
            m_instance.reset(new EventPublisher(queue));
    }
    return  m_instance;
}

void EventPublisher::publish(std::shared_ptr<Event> event)
{
    if (!m_eventQueue)
        return;

    m_eventQueue->push(event);
}

EventPublisher::~EventPublisher()
{

}

EventPublisher::EventPublisher(std::shared_ptr<EventQueue> queue)
{
    m_eventQueue = queue;
}
