#ifndef EVENTPUBLISHER_H
#define EVENTPUBLISHER_H

#include <memory>

class Event;
class EventQueue;
class EventPublisher
{
public:
    static std::shared_ptr<EventPublisher> getInstance(std::shared_ptr<EventQueue> queue = nullptr);
    void publish(std::shared_ptr<Event> event);
    ~EventPublisher();
private:
    EventPublisher(std::shared_ptr<EventQueue> queue);

    std::shared_ptr<EventQueue> m_eventQueue;
    static std::shared_ptr<EventPublisher> m_instance;
};

#endif // EVENTPUBLISHER_H
