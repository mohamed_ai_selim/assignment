#include "Event.h"
#include "EventQueue.h"


EventQueue::EventQueue()
{
    m_queueStatus = false;
}

EventQueue::~EventQueue()
{

}

const bool EventQueue::status()
{
    return m_queueStatus;
}

std::shared_ptr<Event> EventQueue::pop(){
    std::unique_lock<std::mutex> lock(m_mutex);
    std::shared_ptr<Event> x = m_events.front();
    m_events.pop();
    m_queueStatus = !m_events.empty();
    return x;
}


void EventQueue::push(std::shared_ptr<Event> x){
    std::lock_guard<std::mutex> lock(m_mutex);
    m_events.push(x);
    m_queueStatus = !m_events.empty();
}
