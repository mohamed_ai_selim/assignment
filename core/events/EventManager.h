#ifndef EVENTMANAGER_H
#define EVENTMANAGER_H


#include <tuple>
#include <vector>
#include <memory>
#include <functional>
#include <unordered_map>

#include "Event.h"


struct EnumClassHash
{
    template <typename T>
    std::size_t operator()(T t) const
    {
        return static_cast<std::size_t>(t);
    }
};

class EventManager
{
public:
    static std::shared_ptr<EventManager> getInstance();
    ~EventManager();

    void processEvent(std::shared_ptr<Event> event);

    template<class T>
    static void registerMe(Event::Type type, T* caller){

        std::function<void(Event*)> subs = std::bind(&T::notify, caller , std::placeholders::_1);
        mObserversDict.insert(std::make_pair(type, std::make_tuple(reinterpret_cast<void*>(caller), subs)));
    }

    template<class T>
    static void unRegisterMe(T* caller){

        std::unordered_map<Event::Type,  std::tuple<void*, std::function<void(Event*)>>, EnumClassHash>::iterator it;
        for (it = std::begin(mObserversDict); it != std::end(mObserversDict); ++it){
            void* call;
            std::tie(caller, std::ignore) =  it->second;
            if ( static_cast<T*>(call) == caller)
                 mObserversDict.erase(it);
        }
    }

protected:
    EventManager();

private:
    static std::unordered_map<Event::Type,  std::tuple<void*, std::function<void(Event*)>>, EnumClassHash> mObserversDict;
    static std::shared_ptr<EventManager> m_instance ;
};

#endif // EVENTMANAGER_H
