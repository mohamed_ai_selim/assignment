
#include "Event.h"
#include "EventQueue.h"

#include "core/scopes/GlobalScope.h"
#include "EventManager.h"

#include "EventDispatcher.h"

std::shared_ptr<EventDispatcher> EventDispatcher::m_instance = nullptr;

std::shared_ptr<EventDispatcher> EventDispatcher::getInstance()
{
    if (m_instance == nullptr){

        m_instance.reset(new EventDispatcher);
    }
    return  m_instance;

}

EventDispatcher::EventDispatcher()
{

}
void EventDispatcher::processEventQueue(std::shared_ptr<EventQueue> queue)
{
    std::shared_ptr<Event> event = queue->pop();
    //verboseQueue(event);
    scopes::GlobalScope::getInstance()->getEventManager()->processEvent(event);
}



void EventDispatcher::verboseQueue(std::shared_ptr<Event> event)
{
    std::cout << "EventDispatcher::processEventQueue" << std::endl;

    switch (event->getType())
    {
      case Event::Type::TEST:
          std::cout << "Event::Type::TEST"<< std::endl;
          break;
      default:
          break;

    }
}

EventDispatcher::~EventDispatcher()
{

}
