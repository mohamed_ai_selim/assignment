#ifndef EVENTDISPATCHER_H
#define EVENTDISPATCHER_H

#include <memory>

class Event;
class EventQueue;
class EventDispatcher
{
public:

    static std::shared_ptr<EventDispatcher> getInstance();
    void processEventQueue(std::shared_ptr<EventQueue> queue);
    ~EventDispatcher();

private:
    EventDispatcher();
    void verboseQueue(std::shared_ptr<Event> event);
private:

    static std::shared_ptr<EventDispatcher> m_instance;
    //std::weak_ptr<scopes::GlobalScope> m_scope;
};

#endif // EVENTDISPATCHER_H
