#ifndef EVENT_H
#define EVENT_H

class Event
{
public:
    enum class Type{TEST, UPDATE_PROGRESS, ADVANCE_FRAME};

    Event(Type type);
    virtual ~Event();
    virtual const Type getType() const{ return m_type;}
private:
    Type m_type;
};

#endif // EVENT_H
