
#include "EventManager.h"


std::shared_ptr<EventManager> EventManager::m_instance = nullptr;
std::unordered_map<Event::Type,  std::tuple<void*, std::function<void(Event*)>>, EnumClassHash> EventManager::mObserversDict;

std::shared_ptr<EventManager> EventManager::getInstance()
{
    if (m_instance == nullptr)
        m_instance.reset(new EventManager);
    return  m_instance;
}


EventManager::EventManager()
{

}

EventManager::~EventManager()
{

}

void EventManager::processEvent(std::shared_ptr<Event> event)
{

    std::unordered_map<Event::Type,  std::tuple<void*, std::function<void(Event*)>>, EnumClassHash>::iterator it;
    for (it = std::begin(mObserversDict); it != std::end(mObserversDict); ++it){
        if (event->getType() == it->first){
            void* caller;
            std::function<void(Event*)> fn;
            std::tie(caller, fn) =  it->second;
            fn(event.get());
        }
    }
}
