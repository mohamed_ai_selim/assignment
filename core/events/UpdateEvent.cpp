#include "UpdateEvent.h"


UpdateEvent::UpdateEvent(Event::Type type):Event (type)
{

}

UpdateEvent::~UpdateEvent()
{

}

void UpdateEvent::setValue(float val)
{
    m_value = val;
}

float UpdateEvent::getValue() const
{
    return m_value;
}
