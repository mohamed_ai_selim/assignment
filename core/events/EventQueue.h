#ifndef EVENTQUEUE_H
#define EVENTQUEUE_H

#include <queue>
#include <mutex>
#include <vector>
#include <memory>
#include <thread>
#include <atomic>
#include <iostream>
#include <condition_variable>

class Event;

class EventQueue{
public:
    EventQueue();
    ~EventQueue();
    const bool status();
    EventQueue(const EventQueue&) = delete;
    EventQueue& operator= (const EventQueue&) = delete;

    std::shared_ptr<Event> pop();

    void push(std::shared_ptr<Event> x);

private:
    std::queue<std::shared_ptr<Event>> m_events;
    mutable std::mutex m_mutex;
    std::atomic<bool> m_queueStatus;
};

#endif // EVENTQUEUE_H
