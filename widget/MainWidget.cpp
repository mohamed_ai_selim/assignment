// c++ headers
#include <chrono>
#include <iostream>

// Qt
#include <QTimer>
#include <QToolButton>

// global context
#include "core/scopes/GlobalScope.h"

// rendering view
#include "view/SceneViewGL.h"

//simulation classes
#include "simulation/Objects.h"
#include "simulation/SimulationController.h"

// ui  interface
#include "ui_MainWidget.h"
// class header
#include "MainWidget.h"

MainWidget::MainWidget(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    initSimulation();
    initGui();
}

MainWidget::~MainWidget()
{
    m_simulation.reset();
    m_sceneView.reset();
    delete ui;
}

void MainWidget::initGui()
{
    QWidget* container = ui->frameRender;
    m_sceneView.reset(new SceneViewGL(container, m_simulation->getSphere(),  m_simulation->getSurface()));

    QLayout* layout = nullptr;
    if (container->layout()){

        layout = container->layout();
    }
    else {

        layout = new QHBoxLayout(container);
        container->setLayout(layout);
    }
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(m_sceneView.get());

    std::shared_ptr<scopes::GlobalScope> scope = scopes::GlobalScope::getInstance();
    QTimer* timer = scope->getAnimationTimer().get();

    QObject::connect(ui->toolButton_play, &QToolButton::toggled,  this, &MainWidget::handlePlayButtonToggle, Qt::DirectConnection);

    QObject::connect(timer, &QTimer::timeout, this, &MainWidget::handleTimerTimeout, Qt::DirectConnection );

}

void MainWidget::initSimulation()
{
    m_simulation.reset(new SimulationController);

}

void MainWidget::handleTimerTimeout()
{
    m_simulation->advance();
    m_sceneView->update();
}

void MainWidget::handlePlayButtonToggle(bool status)
{
    std::shared_ptr<scopes::GlobalScope> scope = scopes::GlobalScope::getInstance();
    QTimer* timer = scope->getAnimationTimer().get();
    if (status){
        ui->toolButton_play->setText("Pause");
        timer->start();
    }
    else
    {
        ui->toolButton_play->setText("Start");
        timer->stop();
    }
}
