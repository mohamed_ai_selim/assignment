#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <memory>
#include <QMainWindow>
namespace Ui {
    class MainWidget;
}
class SceneViewGL;
class SimulationController;

class MainWidget : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();

private:
    void initGui();
    void initSimulation();
    void handleTimerTimeout();
    void handlePlayButtonToggle(bool status);

private:
    // ui interface
    Ui::MainWidget *ui;
    // sim scene view
    std::shared_ptr<SceneViewGL> m_sceneView;
    // sim controller/computation
    std::shared_ptr<SimulationController> m_simulation;
};

#endif // MAINWIDGET_H
